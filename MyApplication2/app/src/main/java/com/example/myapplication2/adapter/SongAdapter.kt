package com.example.myapplication2.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.provider.BaseColumns
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication2.R
import com.example.myapplication2.SongInfo
import com.example.myapplication2.SongTextShow
import com.example.myapplication2.database.DBManager

class SongAdapter (songList: ArrayList<SongInfo>, context: Context): RecyclerView.Adapter<SongAdapter.SongViewHolder>(){
    private var playList: ArrayList<SongInfo> = songList
    var myContext = context

//    @SuppressLint("NotifyDataSetChanged")
//    fun addItems(items: ArrayList<SongInfo>) {
//        songList = items
////        notifyDataSetChanged()
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongViewHolder{
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_items_song, parent, false)
        return SongViewHolder(v, myContext)
    }

    override fun getItemCount(): Int {
        return playList.size
    }

    override fun onBindViewHolder(holder: SongViewHolder, position: Int) {
        val songs = playList[position]
        holder.bindView(songs)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateAdapter(listItems: List<SongInfo>) {
        playList.clear()
        playList.addAll(listItems)
        notifyDataSetChanged()
    }

    fun removeItem(position: Int, dbManager: DBManager): Int? {
        val status = dbManager.deleteItemFromDB(playList[position].id.toString())
        playList.removeAt(position)
        notifyItemRangeChanged(0, playList.size)
        notifyItemRemoved(position)
        return status
    }

    class SongViewHolder (holderView: View, context: Context) : RecyclerView.ViewHolder(holderView) {

        private val songName = holderView.findViewById<TextView>(R.id.tvSong)
        private val view = holderView
        private val holderContext = context

        @SuppressLint("SetTextI18n")
        fun bindView(song: SongInfo){
            songName.text = "${song.artist} - ${song.songName}"
            view.setOnClickListener{
                val intent = Intent(holderContext, SongTextShow:: class.java).apply{
                    putExtra(IntentConstants.INT_ID_KEY, song.id)
                    putExtra(IntentConstants.INT_ARTIST_KEY, song.artist)
                    putExtra(IntentConstants.INT_SONG_KEY, song.songName)
                    putExtra(IntentConstants.INT_LYRICS_KEY, song.lyrics)
                }
                holderContext.startActivity(intent)
            }
        }
    }
}