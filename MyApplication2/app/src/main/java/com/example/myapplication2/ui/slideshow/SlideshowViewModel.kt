package com.example.myapplication2.ui.slideshow

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SlideshowViewModel : ViewModel() {
    private val _text = MutableLiveData<String>().apply {
        value = "Listening..."
    }
    val text: LiveData<String> = _text
}