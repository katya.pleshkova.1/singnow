package com.example.myapplication2.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns
import com.example.myapplication2.SongInfo

class DBManager(val context: Context){
    private val dbHelper = DBHelper(context)
    var db: SQLiteDatabase? = null

    fun openDB(){
        db = dbHelper.writableDatabase
    }

    fun insertToDB(artist: String, song: String, lyrics: String): Long? {
        val values = ContentValues().apply {
            put(DBName.COLUMN_NAME_ARTIST, artist)
            put(DBName.COLUMN_NAME_SONG, song)
            put(DBName.COLUMN_NAME_LYRICS, lyrics)
        }
        return db?.insert(DBName.TABLE_NAME, null, values)
    }

    @SuppressLint("Range", "Recycle")
    fun readAllData(): ArrayList<SongInfo> {
        val selectQuery = "SELECT * FROM ${DBName.TABLE_NAME}"
        val db = dbHelper.readableDatabase
        val dataList = ArrayList<SongInfo>()
        val cursor : Cursor?
        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: Exception) {
            e.printStackTrace()
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var idData: Int
        var artistName: String
        var songName: String
        var lyrics: String

        if (cursor.moveToFirst())
        {
            do {
                idData = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID))
                artistName = cursor.getString(cursor.getColumnIndex(DBName.COLUMN_NAME_ARTIST))
                songName = cursor.getString(cursor.getColumnIndex(DBName.COLUMN_NAME_SONG))
                lyrics = cursor.getString(cursor.getColumnIndex(DBName.COLUMN_NAME_LYRICS))
                val readSong = SongInfo(idData, artistName, songName, lyrics)
                dataList.add(readSong)
            } while(cursor.moveToNext())
        }
        return dataList
    }

    fun deleteItemFromDB(id: String): Int? {
        val selection = BaseColumns._ID + "=$id"
        return db?.delete(DBName.TABLE_NAME, selection, null)
    }

    fun updateItem(id: Int, artist: String, song: String, lyrics: String): Int? {
        val selection = BaseColumns._ID + "=$id"
        val values = ContentValues().apply {
            put(DBName.COLUMN_NAME_ARTIST, artist)
            put(DBName.COLUMN_NAME_SONG, song)
            put(DBName.COLUMN_NAME_LYRICS, lyrics)
        }
        return db?.update(DBName.TABLE_NAME, values, selection, null)
    }

    fun closeDB(){
        dbHelper.close()
    }
}