package com.example.myapplication2.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context) : SQLiteOpenHelper (context, DBName.DB_NAME,
    null, DBName.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBName.CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, currentVersion: Int) {
        db?.execSQL(DBName.DELETE_TABLE)
        onCreate(db)
    }
}