package com.example.myapplication2.ui.home

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication2.R
import com.example.myapplication2.adapter.SongAdapter
import com.example.myapplication2.database.DBManager
import com.example.myapplication2.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private lateinit var dbManager: DBManager
    private lateinit var recyclerView: RecyclerView
    private var adapter: SongAdapter? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        recyclerView = rootView.findViewById(R.id.songlist) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(activity)
        val swapHelper = getSwap()
        swapHelper.attachToRecyclerView(recyclerView)
        recyclerView.adapter = adapter
//        val button = binding.root.findViewById<Button>(R.id.songs)
//        button.setOnClickListener{
//            parentFragmentManager.beginTransaction().replace(R.id.main_home, Song.newInstance()).commit()
//        }
        return rootView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dbManager = DBManager(context)
        adapter = SongAdapter(ArrayList(), context)
    }

    override fun onResume() {
        super.onResume()
        dbManager.openDB()
        fillAdapter()
    }

    private fun fillAdapter(){
        val playList = dbManager.readAllData()
        Log.e("hhhh","$playList")
        Log.e("pppp", "${playList.size}")
        adapter?.updateAdapter(playList)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        dbManager.closeDB()
        _binding = null
    }

    private fun getSwap(): ItemTouchHelper{
        return ItemTouchHelper(object: ItemTouchHelper.SimpleCallback(0,
            ItemTouchHelper.LEFT)
        {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val status =  adapter?.removeItem(viewHolder.adapterPosition, dbManager)
                if (status != null) {
                    if (status > -1) {
                        Toast.makeText(activity, "Song is successfully removed",
                            Toast.LENGTH_SHORT).show()
                    } else{
                        Toast.makeText(activity, "Error removing song",
                            Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }
}