package com.example.myapplication2

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.textview.MaterialTextView
import org.json.JSONObject
import java.lang.Exception
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class WeatherFragment : Fragment() {

    lateinit var temperature: MaterialTextView
    lateinit var minTemperature: MaterialTextView
    lateinit var maxTemperature: MaterialTextView
    lateinit var humidityView: MaterialTextView
    lateinit var pressureView: MaterialTextView
    lateinit var additional: MaterialTextView
    lateinit var sunriseView: MaterialTextView
    lateinit var sunsetView: MaterialTextView
    lateinit var windView: MaterialTextView
    lateinit var cityName: MaterialTextView
    var city: String = "London"
    var apiKey: String = "959205cda87703bafa75c804100b05cf"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_weather, container, false)
        cityName = root.findViewById(R.id.city)
        cityName.text = city
        temperature = root.findViewById(R.id.temperature)
        minTemperature = root.findViewById(R.id.minTempValue)
        maxTemperature = root.findViewById(R.id.maxTempValue)
        sunsetView = root.findViewById(R.id.sunsetValue)
        sunriseView = root.findViewById(R.id.sunValue)
        humidityView = root.findViewById(R.id.valueHum)
        additional = root.findViewById(R.id.addingInfo)
        windView = root.findViewById(R.id.windValue)
        pressureView = root.findViewById(R.id.valuePress)
        WeatherTask().execute()
        return root
    }

    @SuppressLint("StaticFieldLeak")
    inner class WeatherTask(): AsyncTask<String, Void, String>() {

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg p0: String?): String? {
            val response: String? = try{
                URL("https://api.openweathermap.org/data/2.5/weather?q=$city&units=metric&appid=$apiKey")
                    .readText(Charsets.UTF_8)

            }catch (e: Exception) {
                null
            }
            return response
        }

        @Deprecated("Deprecated in Java")
        @SuppressLint("SetTextI18n")
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                val jsonObj = JSONObject(result)
                val main = jsonObj.getJSONObject("main")
                val sys = jsonObj.getJSONObject("sys")
                val wind = jsonObj.getJSONObject("wind")
                val weather = jsonObj.getJSONArray("weather").getJSONObject(0)
                val updateAt = jsonObj.getLong("dt")
                val temp = main.getLong("temp").toInt()
                val temper = "$temp °C"
                val tempMin = main.getLong("temp_min").toInt()
                val tempMinStr = "$tempMin °C"
                val tempMax = main.getLong("temp_max").toInt()
                val tempMaxStr = "$tempMax °C"
                val pressure = main.getString("pressure") + " P"
                val humidity = main.getString("humidity") + " %"
                val sunrise: Long = sys.getLong("sunrise")
                val sunset: Long = sys.getLong("sunset")
                val windSpeed = wind.getString("speed") + " km/h"
                val weatherDesc = weather.getString("description")
                temperature.text = temper
                minTemperature.text = tempMinStr
                maxTemperature.text = tempMaxStr
                additional.text = weatherDesc
                sunsetView.text = SimpleDateFormat("hh: mm a", Locale.ENGLISH)
                    .format(Date(sunset*1000))
                sunriseView.text = SimpleDateFormat("hh: mm a", Locale.ENGLISH)
                    .format(Date(sunrise*1000))
                windView.text = windSpeed
                humidityView.text = humidity
                pressureView.text = pressure
            }catch (e: Exception){
                Log.e("fff", "Error!")
            }
        }

    }
}