package com.example.myapplication2

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.myapplication2.adapter.IntentConstants
import com.example.myapplication2.database.DBManager
import com.example.myapplication2.databinding.ActivityAddingBinding
import com.google.android.material.textfield.TextInputEditText

class Adding : AppCompatActivity() {
    private lateinit var binding: ActivityAddingBinding
    private val dbManager = DBManager(this)
    private lateinit var artistName: EditText
    private lateinit var songName: EditText
    private lateinit var lyrics: EditText
    private lateinit var btnAdd: Button
    var id = 0
    var isEditState = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adding)
        binding = ActivityAddingBinding.inflate(layoutInflater)
        initView()
        dbManager.openDB()
        getMyIntents()
    }

    private fun initView() {
        artistName = findViewById<TextInputEditText>(R.id.artist_name)
        songName = findViewById<TextInputEditText>(R.id.song_name)
        lyrics = findViewById<TextInputEditText>(R.id.lyrics)
        btnAdd = findViewById<Button>(R.id.add)
    }

    fun saveInfo(view: View)
    {
        val artist = artistName.text.toString()
        val song = songName.text.toString()
        val text = lyrics.text.toString()
        val intent: Intent

        if(isEditState) {
            val statusEdit = dbManager.updateItem(id, artist, song, text)
            if (statusEdit != null)
                if (statusEdit > -1) {
                    Toast.makeText(this, "Song information is successfully updated",
                        Toast.LENGTH_SHORT).show()
                }else {
                    Toast.makeText(this, "Error updating info", Toast.LENGTH_SHORT).show()

                }
            intent = Intent(this, MainActivityMenu::class.java)
            startActivity(intent)
        }
        else
        {
            if (artist.isEmpty() || song.isEmpty()) {
               Toast.makeText(this, "Please input information about song!",
                   Toast.LENGTH_SHORT).show()
            } else {
                val status = dbManager.insertToDB(artist, song, text)
                if (status != null) {
                    if (status > -1) {
                        Toast.makeText(this, "Song is successfully added", Toast.LENGTH_SHORT).show()
                        clearEditText()
                    }else {
                        Toast.makeText(this, "Error saving info", Toast.LENGTH_SHORT).show()

                    }
                }
                intent = Intent(this, MainActivityMenu::class.java)
                startActivity(intent)
            }
        }
        finish()
    }

    private fun clearEditText() {
        artistName.setText("")
        songName.setText("")
        lyrics.setText("")
        artistName.requestFocus()
    }

    @SuppressLint("SetTextI18n")
    private fun getMyIntents(){
        val i = intent
        if (i != null) {
            if (i.getStringExtra(IntentConstants.INT_ARTIST_KEY) != null){
                id = i.getIntExtra(IntentConstants.INT_ID_KEY, 0)
                artistName.setText(i.getStringExtra(IntentConstants.INT_ARTIST_KEY))
                songName.setText(i.getStringExtra(IntentConstants.INT_SONG_KEY))
                lyrics.setText(i.getStringExtra(IntentConstants.INT_LYRICS_KEY))
                isEditState = true
                btnAdd.text = "Save"
            }
        }
    }

    fun exitPage(view: View){
        val intent = Intent(this, MainActivityMenu::class.java)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        dbManager.closeDB()
    }
}