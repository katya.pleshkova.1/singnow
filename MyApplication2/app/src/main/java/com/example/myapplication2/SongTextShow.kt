package com.example.myapplication2

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.BaseColumns
import android.view.View
import android.widget.TextView
import com.example.myapplication2.adapter.IntentConstants
import com.google.android.material.textfield.TextInputEditText

class SongTextShow : AppCompatActivity() {
    private lateinit var artistSongName: TextView
    private lateinit var lyrics: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_text_show)
        initView()
        getMyIntents()
    }

    private fun initView() {
        artistSongName = findViewById<TextInputEditText>(R.id.artistSongName)
        lyrics = findViewById<TextInputEditText>(R.id.songText)
    }

    fun exitPage(view: View){
        val intent = Intent(this, MainActivityMenu::class.java)
        startActivity(intent)
    }

    fun openUpdatePage(view: View) {
        val givenIntent = intent
        val newIntent = Intent(this, Adding:: class.java)
        if (givenIntent != null) {
            newIntent.apply {
                putExtra(IntentConstants.INT_ID_KEY, givenIntent.getIntExtra(IntentConstants.INT_ID_KEY, 0))
                putExtra(IntentConstants.INT_ARTIST_KEY, givenIntent.getStringExtra(IntentConstants.INT_ARTIST_KEY))
                putExtra(IntentConstants.INT_SONG_KEY, givenIntent.getStringExtra(IntentConstants.INT_SONG_KEY))
                putExtra(IntentConstants.INT_LYRICS_KEY, givenIntent.getStringExtra(IntentConstants.INT_LYRICS_KEY))
            }
        }
        startActivity(newIntent)
    }

    @SuppressLint("SetTextI18n")
    fun getMyIntents(){
        val i = intent
        if (i != null) {
            if (i.getStringExtra(IntentConstants.INT_ARTIST_KEY) != null){
                artistSongName.text = "${i.getStringExtra(IntentConstants.INT_ARTIST_KEY)} - ${i.getStringExtra(IntentConstants.INT_SONG_KEY)}"
                lyrics.text = i.getStringExtra(IntentConstants.INT_LYRICS_KEY)
            }
        }
    }
}