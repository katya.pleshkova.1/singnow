package com.example.myapplication2

import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsFragment : Fragment() {

    private lateinit var mMap: GoogleMap

    private val callback = OnMapReadyCallback {
            googleMap ->

        val krasnoyarsk = LatLng(56.01528, 92.89325)
        val shalyapin = LatLng(56.00947723637412, 92.87008489966026)
        val lounge = LatLng(56.011246617521046, 92.86446747782317)
        val monkey = LatLng(55.98819740907903, 92.85512096359038)
        val zoom = 12f
        googleMap.addMarker(MarkerOptions()
            .position(shalyapin)
            .title("Караоке Шаляпин")
            .snippet("Часы работы: пятница, суббота 20:00-04:00"))
        googleMap.addMarker(MarkerOptions()
            .position(lounge)
            .title("Lounge Karaoke Bar RED LIPS")
            .snippet("Часы работы: понедельник-среда 12:00-02:00\n" +
                    "четверг 12:00-00:00\n" +
                    "пятница-суббота 12:00-04:00\n" +
                    "воскресенье 12:00-02:00"))
        googleMap.addMarker(MarkerOptions()
            .position(monkey)
            .title("Monkey bar")
            .snippet("Часы работы: воскресенье-четверг 18:00-02:00\n" +
                    "пятница-суббота 18:00-03:00"))
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(zoom))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(krasnoyarsk))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

//    override fun onMapReady(p0: GoogleMap) {
//        mMap = p0
//        val zoom = 5f
//        val krasnoyarsk = LatLng(56.01528, 92.89325)
//
//        mMap.addMarker(MarkerOptions()
//            .position(krasnoyarsk)
//            .title("Marker in Krasnoyarsk"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(krasnoyarsk))
//        mMap.moveCamera(CameraUpdateFactory.zoomTo(zoom))
//
//    }
}