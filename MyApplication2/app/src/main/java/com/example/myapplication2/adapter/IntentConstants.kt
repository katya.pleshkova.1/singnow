package com.example.myapplication2.adapter

object IntentConstants {

    const val INT_ARTIST_KEY = "artist_key"
    const val INT_SONG_KEY = "song_key"
    const val INT_LYRICS_KEY = "lyrics_key"
    const val INT_ID_KEY = "id_key"

}