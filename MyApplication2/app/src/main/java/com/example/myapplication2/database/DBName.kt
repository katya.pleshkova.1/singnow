package com.example.myapplication2.database

import android.provider.BaseColumns

object DBName: BaseColumns {
//    table
    const val TABLE_NAME = "playlist"
    const val COLUMN_NAME_ARTIST = "artist"
    const val COLUMN_NAME_SONG = "song"
    const val COLUMN_NAME_LYRICS = "lyrics"
//    database
    const val DB_VERSION = 1
    const val DB_NAME = "SingNow.db"

    const val CREATE_TABLE = "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
            "${BaseColumns._ID} INTEGER PRIMARY KEY," +
            "$COLUMN_NAME_ARTIST TEXT," +
            "$COLUMN_NAME_SONG TEXT," +
            "$COLUMN_NAME_LYRICS TEXT" +
            ")"

    const val DELETE_TABLE = "DROP TABLE IF EXISTS $TABLE_NAME"
}